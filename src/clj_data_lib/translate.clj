(ns clj-data-lib.translate
  (:require [clj-data-lib.data :as d]
            [clj-data-lib.time :as ti]
            [clojure.string :as string]))

(defn conversion-error [t v e]
  (ex-info (str "Error converting " t " for type " (type v) " value " v)
           {:conversion-error e}))

(defn type->str
  "Take input of (unknown) type and return formatted string."
  [a]
  (try
    (cond
      (nil? a) nil
      (integer? a) (str a)
      (ti/any-date-type? a) (ti/type->str a "yyyy-MM-dd")
      (ti/any-date-time-type? a) (ti/type->str a)
      :else (str a))
    (catch Exception err
      (throw (Exception. (str "Error attempting to convert input to string for: " a " of type " (type a) ": " err))))))

(defn unk->type
  "Return proper type from unknown/string value."
  [a]
  (cond
    (nil? a) nil
    (and (string? a) (d/str-is-number? a)) (d/parse-number a)
    (and (string? a) (ti/str-is-date-time? a)) (ti/type->local-date-time a)
    (and (string? a) (ti/str-is-date? a)) (ti/type->local-date a)
    (and (string? a) (ti/str-is-inst? a)) (ti/type->local-date-time a)
    :else a))

(defn unk->str
  "Return formatted string from unknown type. Combination method of above."
  [a] (-> a unk->type type->str))

(defn type->int [a]
  (try
    (cond
      (nil? a) nil
      (integer? a) a
      (boolean? a) (if a 1 0)
      (string? a) (Integer/parseInt a)
      :else (int a))
    (catch Exception e
      (throw (conversion-error "type->int" a e)))))

(defn type->float [a]
  (try
    (cond
      (nil? a) nil
      (string? a) (Float/parseFloat a)
      :else (float a))
    (catch Exception e
      (throw (conversion-error "type->float" a e)))))

(defn str->char [d] (if (string? d) (.charAt ^String d 0) d))

(defn bigdecimal->int [v]
  (if (= (type v) BigDecimal) (.intValue ^BigDecimal v) v))

(defn bigdecimal->plainString [^BigDecimal v]
  (-> v (.stripTrailingZeros) (.toPlainString)))

(defn- nom-type->bool [i]
  (cond
    (or (nil? i) (boolean? i)) i
    (or (and (instance? Double i) (some #{i} [0.0 1.0]))
        (and (integer? i) (some #{i} [0 1]))
        (and (instance? BigDecimal i) (some #(= i (bigdec %)) [0 1]))
        (and (string? i) (d/str-is-number? i) (some #(= (bigdec i) (bigdec %)) [0 1])))
    (if (= (bigdec i) (bigdec 1)) true false)
    (and (string? i) (some #(= (string/lower-case i) %) ["true" "t"])) true
    (and (string? i) (some #(= (string/lower-case i) %) ["false" "f"])) false
    :else (throw (Exception. (str "Unable to change type->bool for " i)))))

(def type->bool (memoize nom-type->bool))