(ns clj-data-lib.spec.translate
  (:require [clj-data-lib.translate :as trn]
            [clojure.spec.alpha :as s]))

(s/fdef trn/str->char
  :args (s/or :d (s/and string? #(= (count %) 1))
              :a char?)
  :ret char?)