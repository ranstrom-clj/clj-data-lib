(ns clj-data-lib.spec.time
  (:require [clojure.spec.alpha :as s]
            [clj-data-lib.time :as ti]))

(s/fdef ti/get-instant
  :args (s/cat :offset-minutes integer?)
  :ret ti/java-time-instant?)

(s/fdef ti/instant-as-str
  :args (s/cat :offset-minutes integer?)
  :ret string?)

(s/fdef ti/instant-as-file-friendly-str
  :ret string?)

(s/def ::supported-date-types
  (s/or :java-sql-date ti/java-sql-date?
        :java-sql-timestamp ti/java-sql-timestamp?
        :java-util-date ti/java-util-date?
        :java-time-instant ti/java-time-instant?
        :java-time-date-time ti/java-time-date-time?
        :java-time-date ti/java-time-date?))

(s/fdef ti/any-date-time-type?
  :args (s/cat :a ::supported-date-types)
  :ret boolean?)

(s/fdef ti/str-is-date-time?
  :args (s/cat :a string?)
  :ret boolean?)

(s/fdef ti/str-is-date?
  :args (s/cat :a string?)
  :ret boolean?)

; TODO enable these with error handling due to parsing error

;(s/fdef ti/type->local-date-time
;        :args (s/cat :a any?)
;        :ret (s/or :java-time-date-time ti/java-time-date-time?))
;
;(s/fdef ti/type->local-date-time
;        :args (s/cat :a any?)
;        :ret (s/or :java-time-date ti/java-time-date?))

;(s/fdef ti/type->str
;        :args (s/cat :a any?)
;        :ret (s/or :string string?))
;
;(s/fdef ti/date-str->str
;        :args (s/cat :a string?
;                     :date-format string?)
;        :ret string?)
