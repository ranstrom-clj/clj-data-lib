(ns clj-data-lib.spec.string
  (:require [clj-data-lib.string :as cls]
            [clojure.spec.alpha :as s]))

(s/fdef cls/shorten-utf-8
  :args (s/cat :input-str string?
               :max-bytes integer?)
  :ret string?)

(s/fdef cls/ci=
  :args (s/cat :str1 string?
               :str2 string?)
  :ret boolean?)

(s/fdef cls/ci-in-coll?
  :args (s/cat :str1 string?
               :coll1 (s/nilable (s/coll-of string?)))
  :ret boolean?)