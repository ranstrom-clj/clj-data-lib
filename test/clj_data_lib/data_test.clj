(ns clj-data-lib.data-test
  (:require [clojure.test :refer [deftest is]]
            [clj-data-lib.data :as d]
            [clj-data-lib.fixtures :as fx]))

(deftest test-generated
  ;; (is (fx/qc-test `d/coll-contains?))
  (is (fx/qc-test `d/coll-of-maps?))
  (is (fx/qc-test `d/duplicates))
  (is (fx/qc-test `d/parse-number))
  (is (fx/qc-test `d/str-is-number?)))

;(deftest test-converters-and-type-tests
;  (testing "check if string is integer"
;    (is (d/str-is-number? "1234"))
;    (is (= false (d/str-is-number? "12 34")))
;    (is (d/str-is-number? "12.34"))))