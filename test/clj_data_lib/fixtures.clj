(ns clj-data-lib.fixtures
  (:require [clojure.spec.test.alpha :as stest]))

(defn instrument [f]
  (stest/instrument f))

(defn qc-test [f]
  (stest/instrument f)
  (let [res (:failure (stest/abbrev-result (first (stest/check f))))]
    (when-not (nil? res)
      (println res))
    (nil? res)))