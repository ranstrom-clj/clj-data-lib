(ns clj-data-lib.time-test
  (:require [clj-data-lib.time :as ti]
            [clojure.test :refer [deftest is testing]]
            [clj-data-lib.jt2 :as jt]))

(deftest test-converters-and-type-tests
  (testing "inst something successfully"
    (is (= (ti/type->local-date-time "2018-12-25 17:30:38.3770000")
           (ti/type->local-date-time #inst "2018-12-25T17:30:38.377000000-00:00")))
    (is (= (ti/type->local-date-time "2018-12-25 17:30:38.3770000")
           (jt/local-date-time 2018 12 25 17 30 38 377000000))))
  (testing "check if string is inst"
    (is (ti/str-is-date-time? "2018-12-25 17:30:38.3770000"))
    (is (= false (ti/str-is-date-time? "123456790")))))

(deftest test-instant-as-str
  (testing "retrieving instant as string"
    (is (string? (ti/instant-as-str)))
    (is (string? (ti/instant-as-file-friendly-str)))
    (is (string? (ti/instant-as-str 90))))
  (testing "Other offsets"
    (is (string? (ti/type->str (ti/instant-offset :days -90))))
    (is (string? (ti/type->str (ti/instant-offset :hours -90))))
    (is (string? (ti/type->str (ti/instant-offset :minutes -90))))
    (is (thrown? Exception (ti/type->str (ti/instant-offset :lightyears -90))))))

(deftest test-date-str->str
  (testing "date-str & date-time-str to local-date-time local-date"
    (is (= (ti/date-time-str->local-date-time "7/01/2018 9:02:01 AM" "M/dd/yyyy h:mm:ss a")
           (jt/local-date-time 2018 7 1 9 2 1)))
    (is (= (ti/date-str->local-date "7/01/2018" "M/dd/yyyy")
           (jt/local-date 2018 7 1))))
  (testing "date-str->str"
    (is (= (ti/date-str->str "7/01/2018 9:02:01 AM" "M/dd/yyyy h:mm:ss a")
           "2018-07-01 09:02:01.000"))
    (is (= (ti/date-str->str "11/01/2018 11:02:01 AM" "M/dd/yyyy h:mm:ss a")
           "2018-11-01 11:02:01.000"))
    (is (= (ti/date-str->str "09/28/2015 09:28:20 PM" "MM/dd/yyyy hh:mm:ss a")
           "2015-09-28 21:28:20.000"))
    (is (= (ti/date-str->str "7/1/2018 9:02:01 AM" "M/d/yyyy h:mm:ss a")
           "2018-07-01 09:02:01.000"))
    (is (= (ti/date-str->str "7/1/2018" "M/d/yyyy")
           "2018-07-01 00:00:00.000"))
    ;(is (= (ti/date-str->str "09/28/2015 09:28:20 PM" "MM/dd/yyyy hh:mm:ss a" {:input-time-zone  "America/New_York"
    ;                                                                           :output-time-zone "UTC"})
    ;       "2015-09-29 01:28:20.000"))
    (is (= (ti/date-str->str "7/1/2018" "M/d/yyyy" "MM-dd-yyyy")
           "07-01-2018"))))

(deftest test-time-compare
  (testing "Time comparisons"
    (let [t1 (ti/instant-offset :days -90)
          t2 (ti/instant-offset :days -30)]
      (is (true? (ti/time= t1 t1 t1)))
      (is (false? (ti/time= t2 t1)))
      (is (false? (ti/time= t2 nil)))
      (is (true? (ti/time> t2 t1)))
      (is (false? (ti/time> t1 t2)))
      (is (true? (ti/time< t1 t2)))
      (is (false? (ti/time< t2 t1)))
      (is (true? (ti/time>= t2 t2)))
      (is (true? (ti/time<= t2 t2))))))

(deftest test-sql-date
  (testing "SQL Dates"
    (is (ti/type->sql-date (ti/instant-as-str)))
    (is (ti/type->sql-date-time (ti/instant-as-str)))))

(deftest test-date-ranges
  (testing "Get date ranges"
    (let [start-date (ti/type->local-date "2021-01-01")
          end-date (ti/type->local-date "2021-03-01")]
      (is (= 9 (count (ti/date-ranges start-date end-date :weeks))))
      (is (= 3 (count (ti/date-ranges start-date end-date :months))))
      (is (= 1 (count (ti/date-ranges start-date end-date :years))))
      (is (= 60 (count (ti/date-ranges start-date end-date :days))))))
  (testing "Get date-time ranges"
    (let [start-date (ti/type->local-date "2021-01-01")
          end-date (ti/type->local-date "2021-03-01")]
      (is (= 9 (count (ti/date-time-ranges start-date end-date :weeks))))
      (is (= 3 (count (ti/date-time-ranges start-date end-date :months)))))))

(deftest test-time-is?
  (testing "time-is? time"
    (let [tm (ti/str->local-time "13:00")]
      (is (true? (ti/time-is? tm "13:00")))
      (is (false? (ti/time-is? tm "14:00")))
      (is (true? (ti/time-is? tm ["13:00"])))
      (is (true? (ti/time-is? tm [["12:00" "14:00"]])))
      (is (true? (ti/time-is? tm [["11:00" "12:00"] "13:00"])))))
  (testing "time-is? day of week"
    (let [dt (ti/type->local-date "1892-01-03")]
      (is (true? (ti/time-is? dt "SUNDAY")))
      (is (true? (ti/time-is? dt "SUN")))
      (is (true? (ti/time-is? dt :Sun)))
      (is (true? (ti/time-is? dt :Sunday)))))
  (testing "time-is? month"
    (let [dt (ti/type->local-date "1892-01-03")]
      (is (true? (ti/time-is? dt "JANUARY")))
      (is (true? (ti/time-is? dt "JAN")))
      (is (true? (ti/time-is? dt :Jan)))
      (is (true? (ti/time-is? dt :January))))))
