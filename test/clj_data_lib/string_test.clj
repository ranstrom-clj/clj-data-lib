(ns clj-data-lib.string-test
  (:require [clojure.test :refer [deftest is testing]]
            [clj-data-lib.string :as cls]
            [clj-data-lib.fixtures :as fx]))

(deftest test-generated
  (is (fx/qc-test `cls/ci=))
  (is (fx/qc-test `cls/ci-in-coll?))
  (is (fx/qc-test `cls/shorten-utf8)))

(deftest test-filters-shorten
  (testing "filters: shorten"
    (is (= (cls/shorten "galadriel" "5")
           "galad"))
    (is (= (cls/shorten "galadriel" "50")
           "galadriel"))
    (is (= (cls/shorten "galadriel" 5)
           "galad"))))